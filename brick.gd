extends RigidBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var world = Spatial
# Called when the node enters the scene tree for the first time.
func _ready():
	world = get_parent().get_parent()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func hit(body):
	world.bricks-=1
	print("bricks: ", world.bricks)
	if world.bricks == 0:
		world.createbricks()
	queue_free()


func _on_brick_body_exited(body):
	print("brick says:",body.name)
	#world.currentball._on_ball_body_exited(self)
