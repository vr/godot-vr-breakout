extends RigidBody

var world = Spatial

func _ready():
	world = get_parent()

func _on_ball_body_exited(body):
	print(body.name)
	if "side" in body.name:
		$bounce.play()
	if "brick" in body.name:
		$break.play()
		world.bricks -= 1
		print("bricks: ", world.bricks)
		if world.bricks == 0:
			world.createbricks()
		body.queue_free()
	if body.name == "paddle":
		$paddle.play()
		
	if body.name == "fail":
		world.fail()
		if world.balls == 1:
			world.addball()
		world.balls -= 1
		queue_free()
	else: 
		for n in 3:
			#max speed
			if linear_velocity[n] < -4:
				linear_velocity[n] = -4
			if linear_velocity[n] > 4:
				linear_velocity[n] = 4
			# min speed
			if linear_velocity[n] > -2 and linear_velocity[n] < 2:
				linear_velocity[n] = 2 if linear_velocity[n] > 0 else -2


