
# A simple breakout-style game for Godot-OpenXR.

* Press enter to reset the HMD position
* Press space to start the game
* This program includes an advanced view mode in where the parallax of eye projection can be customized, as well as the inter-pupillar distance and the brightness of each eye screen. This is hidden by default and can be enabled by pressing backspace. Use WASD to set the parallax, keys Q and E to set the IPD, keys Z and X to set the brightness, and P to print current values.

## DISCLAIMER:

The advanced display mode is intended for the research of behavioral optometry. Do not use for therapeutic applications without the supervision of an optometrist. The author declines all responsibility for the use of this program as a therapeutic tool.

## Dependencies:

* Godot OpenXR

    Download using AssetLib or from https://github.com/GodotVR/godot_openxr/releases

