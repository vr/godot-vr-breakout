extends Spatial

var bricks = 0
var balls = 0

var r = RandomNumberGenerator.new()
var ball = Spatial

func _ready():
	r.randomize()
	var VR = ARVRServer.find_interface("OpenXR")
	if VR and VR.initialize():
		get_viewport().arvr = true
		get_viewport().hdr = false
		get_viewport().keep_3d_linear = false
		get_viewport().usage = Viewport.USAGE_3D_NO_EFFECTS

		OS.vsync_enabled = false
		Engine.iterations_per_second = 90
	else:
		OS.vsync_enabled = true

func addball():
	if bricks == 0:
		createbricks()
	var ballmodel = preload("ball.tscn")
	var ball = ballmodel.instance().duplicate()
	ball.apply_central_impulse(Vector3(r.randf_range(-4,4),r.randf_range(-4,4),-2))
	ball.translate(Vector3(0,0,4))
	add_child(ball)
	balls += 1

func addbrick(x, y, z, color):
	var brickmodel = preload("brick.tscn")
	var brick = brickmodel.instance().duplicate()
	brick.translate(Vector3(x, y, z))
	
	var mat = brick.get_child(0)
	var newMaterial = mat.get_active_material(0).duplicate()
	newMaterial.albedo_color = color
	mat.material_override = newMaterial

	add_child(brick)
	bricks += 1

func fail():
	$ARVROrigin/fail.play()

func addlayer(z,color):
	for x in 5:
		for y in 5:
			var separation = 1.5
			var shift = 3
			addbrick(x * separation - shift , y * separation - shift , z   - shift , color)

func _unhandled_key_input(event):
	if event.pressed and event.scancode == KEY_SPACE:
		addball()

func createbricks():
	addlayer(0, Color(0.8, 0.2, 1.0))
	addlayer(1, Color(0.7, 1.0, 0.1))
	addlayer(2, Color(1.0, 0.1, 0.3))
